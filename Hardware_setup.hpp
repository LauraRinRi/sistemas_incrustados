#include <ti/devices/msp432p4xx/inc/msp.h>
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>
#include "HAL_I2C.h"
#include "HAL_OPT3001.h"

#include <stdio.h>

#ifndef HARDWARE_SETUP_HPP_
#define HARDWARE_SETUP_HPP_

#define TIMER32_2_COUNT_1s 0x0002DC6B // ~1s with 3MHz clock and clock divided by 16
#define TIMER32_1_COUNT_1s  0x002DC6BF // ~1s with 3MHz clock and clock divided by 1
#define TIMER32_2_COUNT_60s 0x00ABA94F // ~60s with 3MHz clock and clock divided by 16
#define TIMER32_2_COUNT_30s 0x0055D4A7 // ~30s with 3MHz clock and clock divided by 16

#define MASK(x) (1<< (x))
#define LED_CTRL P2
#define BLUE_LED 0x04
#define GREEN_LED 0x02
#define RED_LED 0x01
#define BLINK_DELAY 0x16E360             //~0.5s delay for the initial blink
#define LIGHT_SENSOR_DELAY 100000       //~33ms delay for light sensor configure

void Initial_blink(int,int);
void InitialConditions_setup(void);
void Pin_setup(void);
void T32_1_setup(void);
void T32_2_setup(void);
void ADC_setup(void);
void LightSensor_setup(void);
void Button_setup(void);



#endif /* HARDWARE_SETUP_HPP_ */
