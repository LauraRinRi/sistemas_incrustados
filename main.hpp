#include <ti/devices/msp432p4xx/inc/msp.h>
#include "HAL_OPT3001.h"

#include "Hardware_setup.hpp"
#include "Interruptions_handler.hpp"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef MAIN_HPP_
#define MAIN_HPP_

float g_fSensorLux;             //Value of the light sensor measurement
bool g_BUnderLightThreshold;    //global bool of light level


#endif /* MAIN_HPP_ */
