#include <ti/devices/msp432p4xx/inc/msp.h>
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef INTERRUPTIONS_HANDLER_HPP_
#define INTERRUPTIONS_HANDLER_HPP_


uint16_t m_u16AudioSampleSum;
int m_iAudioSampleCount;
uint16_t m_u16AudioAverage;     //Storage last 5s average audio

int m_iAudioThreshold;
uint16_t g_LightOnTimerCount;

bool g_BOverAudioThreshold;     //global bool of the audio level
volatile bool m_ButtonFlag;    // show the stage of the blue led


#endif
