#include "Interruptions_handler.hpp"


/*****************************************************************
********************* INTERRUPTS ********************************/
extern "C" {

    /************************************************************/
void T32_INT1_IRQHandler(void) {
        /*Generates an interrupt every 1s: ADC14 starts*/
        __disable_irq();
        TIMER32_1->INTCLR = 0U;

        if((g_LightOnTimerCount<15) & ((P2->OUT & BIT2) == BIT2) ){
            g_LightOnTimerCount++;
        }
        else{
            P2->OUT &= ~BIT2;
            g_LightOnTimerCount = 0u;
        }


        ADC14->CTL0 = ADC14->CTL0 | ADC14_CTL0_SC; // Start

        __enable_irq();
        return;
    }

    /************************************************************/
void T32_INT2_IRQHandler(void) {
    /*P1-Red_LED blinks with 30s period*/

    __disable_irq();
    TIMER32_2->INTCLR = 0U;
    /*************************/

    /*************************/
    __enable_irq();
    return;
    }

    /************************************************************/
void ADC14_IRQHandler(void) {
    /*ADC14 samples the audio level of the room*/
    __disable_irq();

    /************************/

    /************************/

    ADC14->CLRIFGR0 = ADC14_CLRIFGR0_CLRIFG0;
    __enable_irq();
        return;
    }

    /************************************************************/
void PORT1_IRQHandler(void) {
        uint32_t status;
        status = GPIO_getEnabledInterruptStatus ( GPIO_PORT_P1 ) ;

        if ( status & GPIO_PIN4 ) {
            /* Toggle blue LED*/
            GPIO_toggleOutputOnPin( GPIO_PORT_P2 , GPIO_PIN2 ) ;
            m_ButtonFlag = true ;
            GPIO_clearInterruptFlag( GPIO_PORT_P1 , status ) ;
        }
    }
}
