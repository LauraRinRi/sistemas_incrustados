#include "Hardware_setup.hpp"



//*****************************************************************
void Initial_blink(int i_LED_LIGHT, int i_BLINK_TIMES) {
    LED_CTRL->DIR |= i_LED_LIGHT;
    LED_CTRL->OUT |= i_LED_LIGHT;

    for(int i = 0; i < 2*i_BLINK_TIMES; i++ ) {
        __delay_cycles(BLINK_DELAY);
        LED_CTRL->OUT ^= i_LED_LIGHT;
    };
    LED_CTRL->OUT ^= i_LED_LIGHT;
}



//*****************************************************************

void InitialConditions_setup(void){

    P2->OUT &= ~BIT2; //apagar LED azul P2
    P1->OUT &= ~BIT0; //apagar LED rojo P1
    P2->OUT &= ~BIT1; //apagar LED verde P2

    Initial_blink(BLUE_LED,3);

    return;
}

//*****************************************************************
void Pin_setup(void){

    //P2->DIR = BIT0; // Red LED
    P2->DIR |= BIT1; // Green LED- 0x00000010 port P2.1 as an output
    P2->DIR |= BIT2; // Blue LED - 0x00000100 port P2.2 as an output

    P1->DIR = BIT0; // Red LED- 0x00000001 port P1.0 as an output

    // Set P4.3 for Analog input, disabling the I/O circuit.
    P4->SEL0 = BIT3;
    P4->SEL1 = BIT3;
    P4->DIR &= ~BIT3;

    return;
}

//*****************************************************************
void T32_1_setup(void){

    TIMER32_1->LOAD |= TIMER32_1_COUNT_1s;   //TIMER_COUNT = 3000000-1;
    TIMER32_1->CONTROL = TIMER32_CONTROL_SIZE | TIMER32_CONTROL_PRESCALE_0 |
                         TIMER32_CONTROL_MODE | TIMER32_CONTROL_IE | TIMER32_CONTROL_ENABLE;

    /*TIMER32_CONTROL_PRESCALE_0: 0 stages of prescale, clock is divided by 1 */
    NVIC_SetPriority(T32_INT1_IRQn,1);
    NVIC_EnableIRQ(T32_INT1_IRQn);

    return;
}

//*****************************************************************
void T32_2_setup(void){

    TIMER32_2->LOAD |= TIMER32_2_COUNT_1s;
    TIMER32_2->CONTROL = TIMER32_CONTROL_SIZE | TIMER32_CONTROL_PRESCALE_1 |
                         TIMER32_CONTROL_MODE | TIMER32_CONTROL_IE | TIMER32_CONTROL_ENABLE;

    /*TIMER32_CONTROL_PRESCALE_1: 4 stages of prescale, clock is divided by 16 */
    NVIC_SetPriority(T32_INT2_IRQn,1);
    NVIC_EnableIRQ(T32_INT2_IRQn);


    return;
}

//*****************************************************************
void ADC_setup(void){

    ADC14->CTL0 = ADC14_CTL0_PDIV_0 |
                    ADC14_CTL0_SHS_0 |
                    ADC14_CTL0_DIV_7 |
                    ADC14_CTL0_SSEL__MCLK |
                    ADC14_CTL0_SHT0_1 |
                    ADC14_CTL0_ON |
                    ADC14_CTL0_SHP;

    ADC14->MCTL[0] = ADC14_MCTLN_INCH_10 | ADC14_MCTLN_VRSEL_0;

    ADC14->CTL0 = ADC14->CTL0 | ADC14_CTL0_ENC; // set enable
    ADC14->IER0 = ADC14_IER0_IE0;
    NVIC_SetPriority(ADC14_IRQn,1);
    NVIC_EnableIRQ(ADC14_IRQn);
    return;
    }

void LightSensor_setup(void){

    /* Initialize I2C communication */
    Init_I2C_GPIO();
    I2C_init();

    /* Initialize OPT3001 digital ambient light sensor */
    OPT3001_init();

    __delay_cycles(LIGHT_SENSOR_DELAY);

    return;
    }

void Button_setup(void){
        // Configura los LEDS Verde y Azul
        GPIO_setAsOutputPin( GPIO_PORT_P2 , GPIO_PIN2 ) ;

        // Setea los botones como entradas
        GPIO_setAsInputPinWithPullUpResistor( GPIO_PORT_P1 , GPIO_PIN4 ) ;

        // Crea las interrupciones en posedge

        GPIO_interruptEdgeSelect( GPIO_PORT_P1 , GPIO_PIN4 ,GPIO_LOW_TO_HIGH_TRANSITION ) ;
        GPIO_clearInterruptFlag( GPIO_PORT_P1 , GPIO_PIN4 ) ;

        // Habilita las interrupciones en el puerto 1
        GPIO_enableInterrupt( GPIO_PORT_P1 , GPIO_PIN4 ) ;

        // Habilita las interruciones en el vector de interrupciones
        Interrupt_enableInterrupt( INT_PORT1 ) ;

        // Habilita las interrupciones globalmente
        Interrupt_enableMaster() ;

        //  Apaga los LEDS
        GPIO_setOutputLowOnPin( GPIO_PORT_P2 , GPIO_PIN2 ) ;
    }

