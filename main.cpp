#include "main.hpp"


int main(void)
   {
    WDTCTL = WDTPW | WDTHOLD;   /* Stop watchdog timer */

 /***************Hardware Setup***********************/
    Pin_setup();
    T32_1_setup();
    T32_2_setup();
    ADC_setup();
    Button_setup();
    LightSensor_setup();

    //Initial Conditions after power up
    InitialConditions_setup();

    //Reset values
    g_LightOnTimerCount = 0U;
    g_BUnderLightThreshold = false;
    g_BOverAudioThreshold = false;


    while (1)
    {

        /**Define light state **/
        g_fSensorLux = OPT3001_getLux();

        if(g_fSensorLux > 30){
            //if the light is on or if is bright
            g_BUnderLightThreshold = false;
        }
        else{
            //if the place is dark
            g_BUnderLightThreshold = true;

        }
        /******************************************/

        /***********Turn on light conditions***********/
        if(g_BUnderLightThreshold & g_BOverAudioThreshold){

            if((P2->OUT & BIT2) == 0U){
                /* if the audio is higher than the threshold
                 * if the light is under the threshold
                 * if the light is off */

                //P2->OUT = BIT2; //turn on the blue LED
            }
            // if the light is already on, pass
        }
        else{
            //P2->OUT &= ~BIT2; //turn off the blue LED
        }
        /******************************************/
    }
    return 0;
}



